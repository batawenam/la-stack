/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_benin': {
    width: 49,
    height: 30,
    viewBox: '0 0 49 30',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M0 0h18v29.557H0V0z" _fill="#319400"/><path pid="1" fill-rule="evenodd" clip-rule="evenodd" d="M18 0h31v15H18V0z" _fill="#FFD600"/><path pid="2" fill-rule="evenodd" clip-rule="evenodd" d="M18 15h31v15H18V15z" _fill="#DE2110"/>'
  }
})
