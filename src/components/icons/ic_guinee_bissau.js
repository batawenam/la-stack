/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_guinee_bissau': {
    width: 72,
    height: 64,
    viewBox: '0 0 72 64',
    data: '<g filter="url(#filter0_d)"><path pid="0" d="M26 54h32c6.627 0 10-4.925 10-11V33H26v21z" _fill="#00785E"/><path pid="1" d="M58 10H26v23h42V21c0-6.075-3.373-11-10-11z" _fill="#F9CB38"/><path pid="2" d="M26 10H14C7.373 10 4 14.925 4 21v22c0 6.075 3.373 11 10 11h12V10z" _fill="#EC1C24"/><path pid="3" d="M23.06 29.357l-6.303.016-1.95-6.379-1.945 6.38-6.308-.017 5.109 3.883-1.976 6.334 5.124-3.936 5.132 3.936-1.982-6.334 5.099-3.883z" _fill="#25333A"/></g><defs><filter id="filter0_d" x="0" y="0" width="72" height="72" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="4"/><feGaussianBlur stdDeviation="2"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/><feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs>'
  }
})
