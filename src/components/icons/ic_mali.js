/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_mali': {
    width: 64,
    height: 64,
    viewBox: '0 0 64 64',
    data: '<path pid="0" d="M22 10h20v44H22V10z" _fill="#F9CB38"/><path pid="1" d="M10 10C3.373 10 0 14.925 0 21v22c0 6.075 3.373 11 10 11h12V10H10z" _fill="#137A08"/><path pid="2" d="M54 10H42v44h12c6.627 0 10-4.925 10-11V21c0-6.075-3.373-11-10-11z" _fill="#EC1C24"/>'
  }
})
