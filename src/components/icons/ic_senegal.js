/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_senegal': {
    width: 72,
    height: 64,
    viewBox: '0 0 72 64',
    data: '<g filter="url(#filter0_d)"><path pid="0" d="M22 10h28v44H22V10z" _fill="#F9CB38"/><path pid="1" d="M14 10C7.373 10 4 14.925 4 21v22c0 6.075 3.373 11 10 11h10V10H14z" _fill="#137A08"/><path pid="2" d="M58 10H48v44h10c6.627 0 10-4.925 10-11V21c0-6.075-3.373-11-10-11z" _fill="#EC1C24"/><path pid="3" d="M45.11 28.22l-6.679.01-2.074-6.471-2.057 6.47-6.695-.01 5.425 3.95-2.098 6.434 5.442-4 5.437 4-2.101-6.433 5.4-3.95z" _fill="#137A08"/></g><defs><filter id="filter0_d" x="0" y="0" width="72" height="72" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="4"/><feGaussianBlur stdDeviation="2"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/><feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs>'
  }
})
