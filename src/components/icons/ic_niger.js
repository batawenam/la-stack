/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_niger': {
    width: 64,
    height: 64,
    viewBox: '0 0 64 64',
    data: '<path pid="0" d="M0 23h64v18H0V23z" _fill="#fff"/><path pid="1" d="M54 10H10C3.373 10 0 14.925 0 21v2h64v-2c0-6.075-3.373-11-10-11z" _fill="#E05206"/><path pid="2" d="M0 43c0 6.075 3.373 11 10 11h44c6.627 0 10-4.925 10-11v-2H0v2z" _fill="#137A08"/><path pid="3" d="M31.979 40.25a8.291 8.291 0 100-16.582 8.291 8.291 0 000 16.582z" _fill="#E05206"/>'
  }
})
