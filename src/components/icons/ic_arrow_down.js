/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_arrow_down': {
    width: 16,
    height: 10,
    viewBox: '0 0 16 10',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M.793 2.207L2.207.793 8 6.586 13.793.793l1.414 1.414L8 9.414.793 2.207z" _fill="#1A1A1A"/>'
  }
})
