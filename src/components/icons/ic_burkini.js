/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_burkini': {
    width: 64,
    height: 64,
    viewBox: '0 0 64 64',
    data: '<path pid="0" d="M54 54H10C3.373 54 0 49.075 0 43V32h64v11c0 6.075-3.373 11-10 11z" _fill="#128807"/><path pid="1" d="M0 21c0-6.075 3.373-11 10-11h44c6.627 0 10 4.925 10 11v11H0V21z" _fill="#EC1C24"/><path pid="2" d="M41.62 29.07l-7.42.01-2.3-7.506-2.285 7.506-7.425-.01 6.01 4.57-2.327 7.46 6.04-4.637 6.04 4.637-2.338-7.46 6.006-4.57z" _fill="#F9CB38"/>'
  }
})
