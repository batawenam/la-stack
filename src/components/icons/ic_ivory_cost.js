/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_ivory_cost': {
    width: 64,
    height: 44,
    viewBox: '0 0 64 44',
    data: '<path pid="0" d="M22 0h20v44H22V0z" _fill="#E6E7E8"/><path pid="1" d="M10 0C3.373 0 0 4.925 0 11v22c0 6.075 3.373 11 10 11h12V0H10z" _fill="#F7941E"/><path pid="2" d="M54 0H42v44h12c6.627 0 10-4.925 10-11V11c0-6.075-3.373-11-10-11z" _fill="#137A08"/>'
  }
})
