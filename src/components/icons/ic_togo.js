/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'ic_togo': {
    width: 36,
    height: 36,
    viewBox: '0 0 36 36',
    data: '<path pid="0" d="M0 27a4 4 0 004 4h28a4 4 0 004-4v-1.2H0V27zm15.526-6.5H15.5l20.5.1v-5.2H15.526v5.1zM32 5H15.526v5.2H36V9a4 4 0 00-4-4z" _fill="#006A4E"/><path pid="1" d="M15.526 15.333v.067H36v-5.2H15.526v5.133zM.026 20.5L0 25.8h36v-5.2l-20.5-.1H.026z" _fill="#FFCE00"/><path pid="2" d="M4 5C1.879 5 .16 6.656.026 8.743V20.5h15.5V5H4z" _fill="#D21034"/><path pid="3" d="M12.339 11.413H8.885l-.013-.04L7.776 8l-.022.068-1.087 3.345h-3.59l2.859 2.077.045.033-1.067 3.285-.042.129 2.904-2.11 2.903 2.11-1.11-3.414 2.905-2.11h-.135z" _fill="#fff"/>'
  }
})
